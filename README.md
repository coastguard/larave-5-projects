# Laravel 5 Projects


## Goto Open Source Packages
- [Debug Bar](https://github.com/barryvdh/laravel-debugbar) - Debugbar for Debugging
- [IDE Helper](https://github.com/barryvdh/laravel-ide-helper) - PHP Storm Helper with Facades
- [Laravel 5 Generators Extended](https://github.com/laracasts/Laravel-5-Generators-Extended) - Extra CLI Generators
- [CORS](https://github.com/barryvdh/laravel-cors) - For Cross-Origin Resource Sharing Headers
- [Form Builder](https://github.com/kristijanhusak/laravel-form-builder) - Form Builder
- [Flash](https://github.com/laracasts/flash) - Easy Flash/Session Messages
- [Predis](https://github.com/nrk/predis) - Redis Caching Library
- [Flatten](https://github.com/Anahkiasen/flatten) - HTML Caching Layer
- [Sentry](https://github.com/cartalyst/sentry/tree/feature/laravel-5) - Authentication & Authorization System
- [JSON Web Token Auth](https://github.com/tymondesigns/jwt-auth) - JSON Web Token Authentication
- [Cashier](https://github.com/laravel/cashier) - Stripe Package for Subscriptions
- [Ekko](https://github.com/laravelista/Ekko) - Helper that detects active navigation menu item and applies classes

## Frontend Stuff
- [Elixir](http://laravel.com/docs/5.0/elixir) - Laravel comes with an addon on top of Gulp pre-built in, here is a quick example of what a file would look like in order to build out the public assets:

```
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function (mix) {
    mix.sass(['app.scss'], 'public/assets/css')
        .scripts(['vendor/bootstrap/bootstrap.js', 'vendor/fastclick/fastclick.js'], 'public/assets/js/vendor.js')
        .scripts(['vendor/jquery/jquery.min.js'], 'public/assets/js/jquery.js')
        .scripts(['vendor/modernizr/modernizr.min.js'], 'public/assets/js/modernizr.js')
        .copy('resources/assets/fonts', 'public/assets/fonts');
});
```

To build out the assets you can use the following commands. The gulp will just build out everything that you have in the gulpfiles.js in the root of the project, and the `--prodocution` will minify these assets.

```
$ gulp
```

```
$ gulp --production
```